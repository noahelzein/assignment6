import os
from flask import Flask
from flask import render_template
from flask import request
import MySQLdb

application = Flask(__name__)
app = application

def get_db_creds():
    db = os.environ.get("RDS_DB_NAME", None)
    username = os.environ.get("RDS_USERNAME", None)
    password = os.environ.get("RDS_PASSWORD", None)
    hostname = os.environ.get("RDS_HOSTNAME", None)
    return db, username, password, hostname

@app.route('/')
def setup():
    
    db, username, password, hostname = get_db_creds()
        
    db = MySQLdb.connect(
                     	    host=hostname,    
                     	    user=username, 
                     	    passwd=password,
                     	    db=db  
                     	)
    # after successful connection checks to see whether a db exists, if not it creates one
    # also creates a table 
    cur = db.cursor()
    cur.execute('create database if not exists my_db')
    #cur.execute("DROP TABLE Movies;")
    cur.execute("CREATE TABLE IF NOT EXISTS Movies ( id INT NOT NULL AUTO_INCREMENT, year INT, title VARCHAR(50), director VARCHAR(50), actor VARCHAR(50), release_date VARCHAR(20), rating DECIMAL(9,4), PRIMARY KEY (id));")
    db.commit()
    cur.close()
    db.close()
    return render_template('index.html')

@app.route('/add_movie', methods=['POST'])
def insert():
        
        db, username, password, hostname = get_db_creds()
        
        db = MySQLdb.connect(
                     		host=hostname,    
                     		user=username, 
                     		passwd=password,
                     		db=db  
                            )
	cur = db.cursor()
	query = "SELECT * FROM Movies m WHERE m.title = '" + str(request.form['title']) + "';"
	cur.execute(query)
	message = ''
	if not cur.rowcount:
	    values = "(" + request.form['year'] + ",'" + request.form['title'] + "','" \
	    + request.form['director'] + "','" + request.form['actor'] + "','" \
	    + request.form['release_date'] + "', " + str(request.form['rating']) + ");"
	    query = "INSERT INTO Movies(year,title,director,actor,release_date,rating) VALUES" + values
	    cur.execute(query)
	    db.commit()
	    cur.execute("SELECT * FROM Movies;")
	    message = "Movie " + request.form['title'] + " successfully inserted"
	else:
	    message = "Movie " + request.form['title'] + " already exists. "
	    
	cur.close()
	db.close()
	return render_template('index.html', message=message)

@app.route('/update_movie', methods=['POST'])
def update():
	db, username, password, hostname = get_db_creds()
        
        db = MySQLdb.connect(
                             host=hostname,    
                             user=username, 
                             passwd=password,
                             db=db  
                            )
	cur = db.cursor()
	query = "SELECT * FROM Movies m WHERE m.title = '" + str(request.form['title']) + "';"
	cur.execute(query)
	message = ''
	if cur.rowcount:
	    values = "year = " + request.form['year'] + "," + "director = '" + request.form['director'] + "', actor = '" \
	    + request.form['actor'] + "', release_date = '" + request.form['release_date'] + "', rating = " + str(request.form['rating'])

	    condition = "WHERE title = '" + request.form['title'] + "';"
	    query = "UPDATE Movies SET " + values + " " + condition 
	    cur.execute(query)
	    db.commit()
	    message = "Movie " + request.form['title'] + " successfully updated"
	else:
		message = "Movie " + request.form['title'] + " does not exist."

	cur.close()
	db.close()
	return render_template('index.html', message=message)

@app.route('/delete_movie', methods=['POST'])
def delete():
	db, username, password, hostname = get_db_creds()
        
        db = MySQLdb.connect(
                                 host=hostname,    
                                 user=username, 
                                 passwd=password,
                                 db=db  
                             )
	cur = db.cursor()
	query = "SELECT * FROM Movies m WHERE m.title = '" + str(request.form['delete_title']) + "';"
	cur.execute(query)
	message = ''

	if not cur.rowcount:
		message = "Movie with " + str(request.form['delete_title']) + " does not exist"
	else:
		query = "DELETE FROM Movies WHERE title='" + str(request.form['delete_title']) + "';"
		cur.execute(query)
		db.commit()
		message = "Movie " + str(request.form['delete_title']) + " successfully deleted"

	cur.close()
	db.close()
	return render_template('index.html', message=message)

@app.route('/search_movie')
def search_movie():
	db, username, password, hostname = get_db_creds()
        
        db = MySQLdb.connect(
                             host=hostname,    
                             user=username, 
                             passwd=password,
                             db=db  
                            )
	cur = db.cursor()
	query = "SELECT * FROM Movies WHERE actor = '" + str(request.args.get('search_actor')) + "';"
	cur.execute(query)
	message = ''
	if cur.rowcount:
		for row in cur.fetchall():
			message = message + str(row[2]) + ", " + str(row[1]) + ", " + str(row[4]) + "\n"
	else:
		message = "No movies found for actor " + str(request.args.get('search_actor'))

	cur.close()
	db.close()
	return render_template('index.html', message=message)

@app.route('/highest_rating')
def highest_rating():
	db, username, password, hostname = get_db_creds()
        
        db = MySQLdb.connect(
                             host=hostname,    
                             user=username, 
                             passwd=password,
                             db=db  
                            )
	cur = db.cursor()
	message = ''
	query = "SELECT * FROM Movies WHERE rating = (SELECT max(rating) FROM Movies);"
	cur.execute(query)
	if cur.rowcount:
		for row in cur.fetchall():
			message = message + str(row[2]) + ", " + str(row[1]) + ", " + str(row[4])+ ", " + str(row[3]) + ", " + str(row[6]) + "\n"
	else:
		message = "No movies found"	

	cur.close()
	db.close()
	return render_template('index.html', message=message)

@app.route('/lowest_rating')
def lowest_rating():
	db, username, password, hostname = get_db_creds()
        
        db = MySQLdb.connect(
                             host=hostname,    
                             user=username, 
                             passwd=password,
                             db=db  
                            )
	cur = db.cursor()
	message = ''
	query = "SELECT * FROM Movies WHERE rating = (SELECT min(rating) FROM Movies);"
	cur.execute(query)
	if cur.rowcount:
		for row in cur.fetchall():
			message = message + str(row[2]) + ", " + str(row[1]) + ", " + str(row[4]) + ", " + str(row[3]) + ", " + str(row[6]) + "\n"
	else:
		message = "No movies found"	

	cur.close()
	db.close()
	return render_template('index.html', message=message)

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')  
